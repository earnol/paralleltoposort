#pragma once

#include <vector>
#include <memory>
#include <atomic>
#include <algorithm>
#include <tbb/concurrent_vector.h>


struct InputBox
{
  size_t cellsCount;
  size_t maxNumber;
  char maxCharacter;
  size_t ProdjectedCellCount() const
  {
    return maxNumber * (maxCharacter - 'A' + 1);
  }
  size_t Address(size_t num, char c) const
  {
    return (c - 'A') * maxNumber + num - 1; //A1 ==> 0
  }
};

class Cell
{
public:
  typedef Cell *PCell;
  enum State
  {
    UNTOUCHED = 0,
    ORDERED,
    INCALCULATION,
    CALCULATED
  };
  Cell():nodeIdx(std::numeric_limits<size_t>::max()),processedDeps(0)
  {
    column = 'A';
    row = 0;
    value = 0;
    order = 0;
    state.store(UNTOUCHED, std::memory_order_relaxed);
    dependsOn.reserve(25);
    dependees.reserve(25);
  }
  Cell(std::string const &str, size_t idx, InputBox const &box, std::vector<Cell::PCell> &cells);
  void ResolveDepends(std::vector<Cell::PCell> &cells)
  {
    std::transform(std::begin(dependsOn), std::end(dependsOn), std::begin(dependsOn), [](PCell p) { return *(PCell *)p; });
    std::for_each(std::begin(dependsOn), std::end(dependsOn), [this](PCell p) {  p->dependees.push_back(PCell(this)); });
  }

  size_t DependsOnCount() const
  {
    return dependsOn.size();
  }
  size_t DependeesCount() const
  {
    return dependees.size();
  }
  bool  CanBeCalculated(int64_t curOrder) const
  {
    bool can = true;
    for (auto it = std::begin(dependsOn); can && it != std::end(dependsOn); it++)
    {
      if ((*it)->state < ORDERED)
        return false;
      if ((*it)->order >= curOrder)
        return false;
    }
    return true;
  }

  void PrintCell(std::ostream &str) const;

  char column;
  size_t row;
  int64_t value;
  int64_t order;
  size_t nodeIdx;
  std::atomic<size_t> processedDeps;
  std::atomic<State> state;

  std::vector<PCell> dependsOn;
  tbb::concurrent_vector<PCell> dependees;
  static size_t maxRelationSize;
};

