// Brick.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>
#include <fstream>
#include <algorithm>
#include <memory>
#include <chrono>
#include <assert.h>
#include <stdlib.h>
#include "tbb/tbb_config.h"
#include "tbb/global_control.h"
#include "tbb/parallel_for.h"
#include "tbb/parallel_for_each.h"
#include "tbb/flow_graph.h"


#include "ThreadPool.h"
#include "Cell.h"

//ordered vector
struct TieredCalculator
{
  typedef tbb::concurrent_vector<Cell::PCell> Tier;
  std::vector<Tier> tiers;
  TieredCalculator()
  {
    tiers.push_back(Tier());
  }
  bool HasDependentNodes(Tier const &tier)
  {
    for (auto it = std::cbegin(tier); it != std::cend(tier); it++)
    {
      if ((*it)->DependeesCount() > 0)
      {
        return true;
      }
    }
    return false;
  }

  void FillTiers()
  {
    int64_t currentTier = 0;
    while(HasDependentNodes(tiers[currentTier]))
    {
      //add tier
      tiers.push_back(Tier());

      Tier &curTier = tiers[currentTier];
      //next tier
      currentTier += 1;
      Tier &nextTier = tiers[currentTier];
      tbb::parallel_for_each(std::begin(curTier), std::end(curTier), [&nextTier, currentTier](Cell::PCell& p)
      {
        for (auto depIt = std::begin(p->dependees); depIt != std::end(p->dependees); depIt++)
        {
          if ((*depIt)->CanBeCalculated(currentTier))
          {
            //assert((*depIt)->state == Cell::UNTOUCHED);
            (*depIt)->order = currentTier;
            Cell::State state = Cell::UNTOUCHED;
            bool exch = (*depIt)->state.compare_exchange_strong(state, Cell::ORDERED);
            if(exch)
            {
              assert(p->order < (*depIt)->order);
              nextTier.push_back(*depIt);
            }            
          }
        }
      });
    } 
  }
  
  template <typename Func>
  void CalculateTiers(Func calcF)
  {
    if (tiers.size() == 0)
      return;
    for (auto tierIt = std::begin(tiers) + 1; tierIt != std::end(tiers); tierIt++)
    {
      //calculate all tiers one by one
      tbb::parallel_for_each(std::begin(*tierIt), std::end(*tierIt), [&calcF](Cell::PCell& p) {calcF(p); });
    }    
  }
};


InputBox BoxEstimateInput(char const * path)
{
  std::string line;
  std::ifstream inpFile(path);
  InputBox box{ 0, 0, 'A' };

  while (std::getline(inpFile, line))
  {
    assert('A' <= line[0] && line[0] <= 'Z');
    assert('0' <= line[1] && line[1] <= '9');
    box.maxCharacter = std::max(line[0], box.maxCharacter);
    box.maxNumber = std::max(strtoull(line.data() + 1, NULL, 10), box.maxNumber);
    box.cellsCount += 1;
  }
  return box;
}

//parse file - fill the cells array, return cells parsed
size_t  ParseFile(char const * path, InputBox const &box,  std::vector<Cell::PCell> &cells, TieredCalculator::Tier &zeroTier, TieredCalculator::Tier &finalTier)
{
  std::string line;
  std::ifstream inpFile(path);
  size_t cellsCount = 0;

  while (std::getline(inpFile, line))
  {
    assert('A' <= line[0] && line[0] <= 'Z');
    assert('0' <= line[1] && line[1] <= '9');
    Cell::PCell p(new Cell(line, cellsCount, box, cells));
    assert(p->row <= box.maxNumber);
    assert(p->column <= box.maxCharacter);
    cells[box.Address(p->row, p->column)] = p;
    cellsCount += 1;
  }
  tbb::parallel_for_each(std::begin(cells), std::end(cells), [&cells, &zeroTier](Cell::PCell& p)
  {
    if (p)
    {
      p->ResolveDepends(cells);
      if (p->DependsOnCount() == 0)
      {
        zeroTier.push_back(p);
      }
    }
  });
  tbb::parallel_for_each(std::begin(cells), std::end(cells), [&cells, &finalTier](Cell::PCell& p)
  {
    if (p)
    {     
      if (p->DependeesCount() == 0)
      {
        finalTier.push_back(p);
      }
    }
  });  
  return cellsCount;
}

void DFCalcStep(Cell::PCell cell, std::vector<Cell::PCell> *l)
{
  if (cell == nullptr)
    return;
  assert(cell->DependsOnCount() == 0 || cell->state != Cell::ORDERED);
  if (cell->state == Cell::INCALCULATION)
  {
    return;
  }
  cell->state = Cell::ORDERED;
  auto caller = std::bind(DFCalcStep, std::placeholders::_1, l);
  std::for_each(std::begin(cell->dependees), std::end(cell->dependees), caller);
  l->push_back(cell);
  cell->state = Cell::INCALCULATION;
}
void DFCalc(std::vector<Cell::PCell> &nodes)
{
  std::vector<Cell::PCell> l;
  auto caller = std::bind(DFCalcStep, std::placeholders::_1, &l);
  std::for_each(std::begin(nodes), std::end(nodes), caller);
  std::for_each(std::rbegin(l), std::rend(l), [](Cell::PCell cell)
  {
    int64_t v = 0;
    std::for_each(std::rbegin(cell->dependsOn), std::rend(cell->dependsOn), [&v](Cell::PCell c) {assert(c->state = Cell::CALCULATED);  v += c->value; });
    cell->value += v;
    cell->state = Cell::CALCULATED;
  });
}


class ConcurrentGraphVisitorTask: public tbb::task 
{
  Cell::PCell cell;
  std::atomic<size_t>  *nodesCounter;
public:
  //constructor
  ConcurrentGraphVisitorTask(Cell::PCell argCell, std::atomic<size_t>  *argParent) : cell(argCell), nodesCounter(argParent){ }
  //executing task
  task* execute() override 
  {
    //it should be ready for visit
    auto lambda = [this](Cell::PCell c)
    {
      size_t v = c->processedDeps.fetch_add(1);
      assert((v + 1) <= c->DependsOnCount());
      if ((v + 1) == c->DependsOnCount())
      {
        tbb::task *next = new(allocate_root()) ConcurrentGraphVisitorTask(c, nodesCounter);
        assert(next != nullptr);
        spawn(*next);
      }
    };
    //visit
    std::for_each(std::begin(cell->dependsOn), std::end(cell->dependsOn), [this](Cell::PCell c)
    {
      assert(c->state.load() == Cell::CALCULATED);
      this->cell->value += c->value;
    });
    this->cell->state.store(Cell::CALCULATED);
    //children
    if (false)//(cell->DependeesCount() > 500)
    {
      //parallel
      tbb::parallel_for_each(std::begin(cell->dependees), std::end(cell->dependees), lambda);
    }
    else
    {
      std::for_each(std::begin(cell->dependees), std::end(cell->dependees), lambda);
    }
    nodesCounter->fetch_sub(1);
    return 0;
  }
};

void calculateDirect(size_t cellsCount, TieredCalculator::Tier &tier)
{
  tbb::task_list list;
  std::atomic<size_t> nodesProcessed(cellsCount - 1);
  std::for_each(std::begin(tier), std::end(tier), [&list, &nodesProcessed](Cell::PCell &c)
  {
    assert(c->state == Cell::ORDERED);
    tbb::task *t = new(tbb::task::allocate_root()) ConcurrentGraphVisitorTask(c, &nodesProcessed);
    list.push_back(*t);
  });
  tbb::task::spawn_root_and_wait(list); 
  size_t current = 0;
  while (!nodesProcessed.compare_exchange_strong(current, 0))
  {
    current = 0;
  }
}

//DAG flow
typedef tbb::flow::continue_node< tbb::flow::continue_msg > CalcNode;
typedef tbb::flow::continue_msg CalcMessage;
void CreateGraph(std::vector<Cell::PCell> cells, size_t cellsCount, tbb::flow::graph &g, CalcNode* &nodes)
{
  //create all nodes in parallel
  tbb::parallel_for_each(std::begin(cells), std::end(cells), [&g, &nodes, &cells](Cell::PCell c)
  {
    if (c)
    {
      new (nodes + c->nodeIdx) CalcNode(g, [c](CalcMessage const &)
      { 
        std::for_each(std::begin(c->dependsOn), std::end(c->dependsOn), [c](Cell::PCell dc)
        {
          assert(dc->state.load() == Cell::CALCULATED);
          c->value += dc->value;
        });
        c->state.store(Cell::CALCULATED);
      });      
    }
  });
  //create all edges
  tbb::parallel_for_each(std::begin(cells), std::end(cells), [&g, &nodes, &cells](Cell::PCell c)
  {
    if (c)
    {
      std::for_each(std::begin(c->dependsOn), std::end(c->dependsOn), [c, &nodes](Cell::PCell dc)
      {
        tbb::flow::make_edge(nodes[dc->nodeIdx], nodes[c->nodeIdx]);
      });
    }
  });
}

void GraphCalculate(tbb::flow::graph &g, CalcNode * nodes, TieredCalculator::Tier &zeroTier)
{
  tbb::parallel_for_each(std::begin(zeroTier), std::end(zeroTier), [&nodes](Cell::PCell c)
  {
    nodes[c->nodeIdx].try_put(tbb::flow::continue_msg());
  });

  g.wait_for_all();
}



int main()
{
  std::shared_ptr<tbb::global_control> pGlobControl(new tbb::global_control(tbb::global_control::max_allowed_parallelism, std::thread::hardware_concurrency()));

  //InputBox box = BoxEstimateInput("input.txt"); 435205
  //In general input box should be known in advance so we do not need to calculate it: we can use compressed vector to save memory
  InputBox box{  0, 99999 , 'V' };
  std::vector<Cell::PCell> initCells(box.ProdjectedCellCount());
  initCells.resize(box.ProdjectedCellCount());
  std::fill(initCells.begin(), initCells.end(), nullptr);

  TieredCalculator tiers;
  TieredCalculator::Tier finalTier;
  size_t cellsCount = ParseFile("input.txt", box, initCells, tiers.tiers[0], finalTier);
  tbb::flow::graph g;
  CalcNode * nodes = (CalcNode *)malloc(sizeof(CalcNode) * cellsCount);
  CreateGraph(initCells, cellsCount, g, nodes);
  std::cout << "Parsing done" << std::endl;

  int mode = 3;

  if (mode == 1)
  {
    tiers.FillTiers();
    std::cout << "Preprocessing done" << std::endl;


    size_t numThreads = std::thread::hardware_concurrency();
    ThreadPool pool(numThreads);
    auto start = std::chrono::steady_clock::now();
    //calculate network
    tiers.CalculateTiers([](Cell::PCell& p)
    {
      for (auto it = std::begin(p->dependsOn); it != std::end(p->dependsOn); it++)
      {
        p->value += (*it)->value;
      }
    });
    auto end = std::chrono::steady_clock::now();
    std::cout << "Calculation time parallel mode: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " microseconds" << std::endl;
  }
  else if(mode == 0)
  {
    auto start = std::chrono::steady_clock::now();
    //calculate network
    DFCalc(initCells);
    auto end = std::chrono::steady_clock::now();
    std::cout << "Calculation time mode 1 thread: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " microseconds" << std::endl;
  }
  else if (mode == 2)
  {
    auto start = std::chrono::steady_clock::now();
    //calculate network
    calculateDirect(cellsCount, tiers.tiers[0]);
    auto end = std::chrono::steady_clock::now();
    std::cout << "Calculation time mode 2 tasks: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " microseconds" << std::endl;
  }
  else if (mode == 3)
  {
    auto start = std::chrono::steady_clock::now();
    //calculate network
    GraphCalculate(g, nodes, tiers.tiers[0]);
    auto end = std::chrono::steady_clock::now();
    std::cout << "Calculation time mode 3 tasks: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " microseconds" << std::endl;
  }
  //output
  std::ofstream outFile("output.txt");
  std::for_each(std::begin(initCells), std::end(initCells), [&outFile](Cell::PCell& p)
  {
    if (p)
    {
      p->PrintCell(outFile);
    }
  });
  free(nodes);

  //clean up
  tbb::parallel_for_each(std::begin(initCells), std::end(initCells), [&initCells](Cell::PCell& p)
  {
    if (p)
      delete p;
  });
}



