#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <assert.h>

#include "Cell.h"


size_t Cell::maxRelationSize = 25;

Cell::Cell(std::string const &str, size_t idx, InputBox const &box, std::vector<Cell::PCell> &cells):nodeIdx(idx), processedDeps(0)
{
  //do init
  order = 0;
  state = UNTOUCHED;

  //parse self
  column = toupper(str[0]);
  char * pEnd, *p;
  row = strtoull(str.data() + 1, &pEnd, 10);
  //now scan dependees out
  p = pEnd;
  while (*p != '\0')
  {
    //skip to beginning
    while (*p != '\0' && !isalpha(*p) && !isdigit(*p))
    {
      p++;
    }
    if(*p == '\0')
      break;
    //we found value
    if (isdigit(*p))
    {
      value += strtoull(p, &pEnd, 10);
      p = pEnd;
      continue;
    }
    char depCol = toupper(*p); p++;
    size_t depRow = strtoull(p, &pEnd, 10);
    p = pEnd;
    size_t addr = box.Address(depRow, depCol);
    //point to place instead of object as object is unresolved
    dependsOn.push_back((Cell::PCell)&cells[addr]);
  }
  //may be we do know the order?
  if (dependsOn.size() == 0)
  {
    state = ORDERED;
    order = 0;
  }
}

void Cell::PrintCell(std::ostream &str) const
{
  assert(state == CALCULATED);
  str << column << row << " = " << value << std::endl;

}